CREATE TABLE
  `post` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `titolo` varchar(255) DEFAULT NULL,
    `contenuto` varchar(255) DEFAULT NULL,
    `id_utente` int(10) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci

  CREATE TABLE
  `utenti` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(255) DEFAULT NULL,
    `password` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci