<?php session_start();
include 'dbconn.php';
$msg = '';

if(isset($_SESSION['user'])) {
    header('Location: index.php');
    die();
}
if (isset($_POST['login'])) {
    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';
    $cpassword = $_POST['cpassword'] ?? '';

    $sql = "SELECT * FROM ".$dbname.".utenti WHERE username = :username";
    $check = $db->prepare($sql);
    $check->bindParam(':username', $username, PDO::PARAM_STR);
    $check->execute();

    $user = $check->fetch(PDO::FETCH_ASSOC);
    
    if(!$user) {
        if( strcmp($password, $cpassword) == 0 ) {
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $sql = "INSERT INTO ".$dbname.".utenti (username, password) VALUES (?, ?)";
            $stmt = $db -> prepare($sql);
            $stmt -> execute([$username, $hash]);

            $_SESSION['user'] = $username;
            header('Location: index.php');
            die();

        } else {
            $msg = "Le due password non corrispondono";
        }
    } else {
        $msg = "L'username è già esistente";
    }

}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Registrazione</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap"
      rel="stylesheet"
    />
    <link href="css/index.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div
      class="bg-[#f2f2f2] min-h-screen w-full flex justify-center items-center"
    >
      <div
        class="flex flex-col sm:flex-row sm:items-center h-full w-full justify-center p-6"
      >
        <div class="flex flex-col space-y-6 justify-center items-center h-full">
          <form
            action=""
            method="POST"
            class="bg-[#fff] shadow-sm p-6 flex-col justify-center items-center space-y-1"
          >
            <h1 class="mont text-3xl">PHP Form Register</h1>
            <div class="flex flex-col space-y-2">
              <div class="flex flex-col">
                <label class="mont">Username</label>
                <input
                  type="text"
                  class="border-2 p-2"
                  placeholder="pippino"
                  name="username"
                />
              </div>
              <div class="flex flex-col">
                <label class="mont">Password</label>
                <input
                  type="password"
                  class="border-2 p-2"
                  placeholder="amoIpanGocciole2022@"
                  name="password"
                />
              </div>
              <div class="flex flex-col">
                <label class="mont">Conferma password</label>
                <input
                  type="password"
                  class="border-2 p-2"
                  placeholder="amoIpanGocciole2022@"
                  name="cpassword"
                />
              </div>
              <?php if(!empty($msg)) echo '<p style="color: red">'.$msg.'</p>' ?>
              <button
                name="login"
                type="submit"
                class="p-2 bg-sky-500 hover:bg-sky-600 text-white"
              >
                Registrati
              </button>
            </div>
          </form>
          <div
            class="flex w-full flex-col space-y-6 justify-center items-center"
          >
            <a href="/login.php" class="text-sky-500 underline">
             hai già un account? accedi</a
            >
            <a
              href="javascript:visibleSnippet()"
              class="text-white bg-sky-500 p-2 px-4 text-center rounded-full"
            >
              curioso di come è stato creato ?
            </a>
          </div>
        </div>
        <div id="snippet" class="hidden sm:w-1/2">
        <script src="https://gitlab.com/giovannipoma28/php/-/snippets/2483359.js"></script>
        </div>
      </div>
    </div>

    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        function visibleSnippet() {
            var snippet = document.getElementById("snippet");
            snippet.classList.remove("hidden");
        }
    </script>
  </body>
</html>


