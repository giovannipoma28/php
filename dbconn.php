<?php
try {
    $hostname = "localhost";
    $dbname = "progettophp";
    $user = "root";
    $pass = "";
    $db = new PDO ("mysql:host=$hostname;dbname=$dbname", $user, $pass);

    //connessione persistente fai così
    /*$db = new PDO("mysql:host=$hostname;dbname=$dbname", $user, $pass, array(
        PDO::ATTR_PERSISTENT => true
    ));*/

} catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>