<?php session_start();
include 'dbconn.php';

if (!isset($_SESSION['user'])) {
    header('Location: login.php');
    die();
} else {
    $username = $_SESSION['user'];
    $sql = "SELECT * FROM " . $dbname . ".utenti WHERE username = :username";
    $check = $db->prepare($sql);
    $check->bindParam(':username', $username, PDO::PARAM_STR);
    $check->execute();
    $user = $check->fetch(PDO::FETCH_ASSOC);
}

if (isset($_POST['password-change-submit'])) {
    $new_password = $_POST['password-change-password'];
    $username = $_SESSION['user'];
    $hashed_password = password_hash($new_password, PASSWORD_DEFAULT);

    $sql = "UPDATE " . $dbname . ".utenti SET password = ? WHERE username = ?";
    $stmt = $db->prepare($sql)->execute([$hashed_password, $username]);

    header('Location: logout.php');
    die();
} 
if (isset($_POST['create-post-submit'])) {
    try {
        $title = $_POST['post-title'];
        $content = $_POST['post-content'];

        $sql = "INSERT INTO " . $dbname . ".post (titolo, contenuto, id_utente) VALUES (?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute([$title, $content, $user['id']]);
    } catch (Exception $e) {
        header('Location: error.php');
    }
}

$sql =
    "SELECT post.titolo as post_title, post.contenuto as post_content, utenti.username, utenti.id as user_id, post.id
    FROM post
    INNER JOIN utenti
    WHERE post.id_utente = utenti.id
    ORDER BY post.id DESC";

    $stmt = $db->query($sql);
    $post_list = $stmt->fetchAll();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Post</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet" />
</head>
<link href="src/index.css" rel="stylesheet" type="text/css" />
<script src="https://kit.fontawesome.com/0646fc0e46.js" crossorigin="anonymous"></script>
</head>

<body>
    <div class="flex justify-center w-full bg-[#f2f2f2] min-h-screen p-6 ">
        <div class="container flex flex-col space-y-8 h-full ">
            <!--navbar-->
            <div class="flex w-full h-1/5 justify-between">
                <h1 class="text-3xl mont font-bold">POST</h1>
                <a href="logout.php" class="bg-sky-500 p-2 rounded-full text-white text-xl ">Logout</a>
            </div>
            <!--body-->
            <div class="w-full flex flex-col lg:flex-row space-x-0 lg:space-x-5 justify-between">
                <form class="w-full" method="POST" action="" >
                    <div class=" w-full lg:w-1/2 h-[80vh] flex flex-col space-y-5">
                        <div class=" w-full h-4/5 relative">
                            <div class="w-full h-full bg-white p-6 rounded-xl hidden absolute bottom-0 space-y-6" id="post">
                                <div class="space-y-2">
                                    <div class="flex flex-col space-y-3">
                                        <label class="mont text-3xl font-bold">Titolo</label>
                                        <input required maxlength="50" type="text" name="post-title" class="outline-none bg-gray-300 rounded-full p-2" placeholder="Ciao Belli !!">
                                    </div>
                                    <div class="flex flex-col space-y-3">
                                        <label class="mont text-3xl font-bold">Contenuto</label>
                                        <textarea required maxlength="255" cols="50" name="post-content" rows="10" class="outline-none resize-none bg-gray-300 rounded-xl p-2" placeholder="Daje Roma Sempre!!, el colosseo è la cosa più bella a Roma"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-col h-full space-y-4 overflow-y-auto">
                                <?php foreach ($post_list as $post) { ?>
                                    <div class="w-full bg-white p-6 rounded-xl">
                                        <h1 class="text-3xl mont"><?= $post['post_title']  ?></h1>
                                        <p class="text-lg"><?= $post['post_content'] ?></p>
                                        <span>creato da: <?= $post['username'] ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="w-full mont flex-row mt-2 ">
                            <button type="submit" id="btnPost" class="bg-sky-500 p-2 px-4 text-white rounded-full hidden" name="create-post-submit">Post</button>
                            <a href="javascript:visibleFormPost()" class="w-full bg-black rounded-full px-4 p-2 text-center text-white">Apri/chiudi editor post</a>
                        </div>

                    </div>
                </form>
                <div class="w-full lg:w-1/2 flex-col flex justify-start space-y-6 items-center">
                    <form method="POST" action="" class="w-full flex flex-col justify-center items-start space-y-2 bg-white p-6 rounded-xl">
                        <h1 class="text-3xl font-bold mb-5">Profilo Utente</h1>
                        <div class="flex w-full justify-between items-center">
                            <span>Profilo utente: </span>
                            <span><?= $_SESSION['user'] ?></span>
                        </div>
                        <div class="flex w-full justify-between items-center">
                            <span>Cambio password</span>
                            <input type="password" minlength="4" maxlength="100" name="password-change-password" class=" outline-none bg-gray-300 w-full p-2 rounded-full" placeholder="nuova pass" />
                        </div>
                        <button type="submit" name="password-change-submit" class="bg-sky-500 p-2 w-full rounded-full text-white">Salva</button>
                    </form>
                    <a href="javascript:visibleSnippet()" class="text-white bg-sky-500 p-2 px-4 text-center rounded-full">
                        curioso di come è stato creato ?
                    </a>
                    <div id="snippet" class="hidden w-full">
                    <script src="https://gitlab.com/giovannipoma28/php/-/snippets/2483359.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
<script>
    
    let postVisible = true

    function visibleSnippet() {
        var snippet = document.getElementById("snippet");
        snippet.classList.remove("hidden");
    }

    function visibleFormPost() {
        var form = document.getElementById("btnPost")
        var btn = document.getElementById("post");
        if (postVisible) {
            form.classList.remove("hidden");
            btn.classList.remove("hidden");
            postVisible = false
        } else {
            postVisible = true
            form.classList.add("hidden");
            btn.classList.add("hidden");
        }
    }

    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
<script src="https://cdn.tailwindcss.com"></script>

</html>