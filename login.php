<?php session_start();
include 'dbconn.php';
$msg = '';

if (isset($_SESSION['user'])) {
  header('Location: index.php');
  die();
}
if (isset($_POST['login'])) { //controlla se il bottone submit è stato cliccato, se sì, esegue ciò che c'è sotto
  $username = $_POST['username'] ?? ''; //username viene preso dal form tramite l'attributo "name" dell'input
  $password = $_POST['password'] ?? ''; //la password viene presa dal form con la stessa modalità

  //la prima query, che ci servirà per capire se l'username esiste nel database
  $sql = "SELECT * FROM " . $dbname . ".utenti WHERE username = :username"; //sql
  $check = $db->prepare($sql); //prepariamo la query
  $check->bindParam(':username', $username, PDO::PARAM_STR);
  //sostituiamo il parametro ":username" nella stringa dell'sql con il vero valore, 
  //cioè quello di $username, che abbiamo appunto preso dal form.

  $check->execute(); //eseguiamo la query

  $user = $check->fetch(PDO::FETCH_ASSOC); //diamo a $user il valore di ciò che è stato
  //preso dalla query, in questo caso può essere un solo valore

  //se l'utente non esiste oppure se la password inserita è diversa da quella che ha l'utente
  //nel caso in cui esso non esista, $msg prende il valore del messaggio d'errore
  if (!$user || !password_verify($password, $user['password'])) $msg = 'Username o password non corretti';
  else { //nel caso in cui sia tutto corretto, si può procedere al login
    $_SESSION['user'] = $username; //mettiamo l'utente nella sessione e lo logghiamo
    header('Location: index.php'); //indirizziamo verso la pagina principale una volta loggati
    die(); //usciamo dallo script
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Login</title>
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet" />
</head>
<link href="css/index.css" rel="stylesheet" type="text/css" />

<body>
  <div class="bg-[#f2f2f2] min-h-screen flex justify-center items-center">
    <div class="flex h-full justify-center items-center  flex-col container p-6 space-y-2">
      <div class="flex flex-col space-y-6 items-center">
        <form method="POST" action="" class="bg-[#fff] shadow-xl p-6 flex-col justify-center items-center space-y-6">
          <h1 class="mont text-3xl">PHP form</h1>
          <div class="flex flex-col space-y-2">
            <div class="flex flex-col">
              <label class="mont">Email</label>
              <input required type="text" class="border-2 p-2" placeholder="pippino" name="username" />
            </div>
            <div class="flex flex-col">
              <label class="mont">Password</label>
              <input required type="password" class="border-2 p-2" placeholder="amoIpanGocciole2022@" name="password" />
            </div>
            <?php if (!empty($msg)) echo '<p style="color: red">' . $msg . '</p>' ?>
            <button name="login" type="submit" class="p-2 bg-sky-500 hover:bg-sky-600 text-white">
              Accedi
            </button>
          </div>
        </form>
        <div class="flex w-full flex-col space-y-6 justify-center items-center">
          <a href="registration.php" class="text-sky-500 underline">
            non hai un account? registrati</a>
          <a href="javascript:visibleSnippet()" class="text-white bg-sky-500 p-2 px-4 text-center rounded-full">
            curioso di come è stato creato ?
          </a>
        </div>
      </div>
      <div id="snippet" class="hidden sm:w-1/2">
        <script src="https://gitlab.com/giovannipoma28/php/-/snippets/2483359.js"></script>
      </div>
    </div>
  </div>
  <script src="https://cdn.tailwindcss.com"></script>
  <script>
    function visibleSnippet() {
      var snippet = document.getElementById("snippet");
      snippet.classList.remove("hidden");
    }
  </script>
</body>

</html>